package edu.uoc.xriusb.tfg.proves;

import java.util.List;
import java.util.Iterator;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import edu.uoc.xriusb.tfg.jpa.MsUser;




@Named
@RequestScoped
public class ProvaPostgre {
		   
	    public ProvaPostgre() {			
		}

		public List<MsUser> listAllUsers() {
			System.out.println( "Hello listUsers!" );
			final EntityManagerFactory emf = 
	                Persistence.createEntityManagerFactory("MediSearch");

	        EntityManager entityManager = emf.createEntityManager();	           
	        
	        @SuppressWarnings("unchecked")
			List<MsUser> allUsers = entityManager.createQuery("from MsUser").getResultList();
			for (Iterator<MsUser> iter = allUsers.iterator(); iter.hasNext();)
			{
				MsUser u = (MsUser) iter.next();
				//System.out.println(u.getEmailAddress());		
			}
			emf.close();
			return allUsers;
			
		}
}
