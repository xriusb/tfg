package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class OfferedTestId implements Serializable {

	private static final long serialVersionUID = 1L;

	private String medicalTestName;
	private String healthCenterName;
	
	public OfferedTestId() {
		super();
	}

	public String getMedicalTestName() {
		return medicalTestName;
	}

	public void setMedicalTestName(String medicalTestName) {
		this.medicalTestName = medicalTestName;
	}

	public String getHealthCenterName() {
		return healthCenterName;
	}

	public void setHealthCenterName(String healthCenterName) {
		this.healthCenterName = healthCenterName;
	}
	@Override
	public boolean equals(Object obj){
		
		if(obj instanceof OfferedTestId){
			OfferedTestId offeredTestsId = (OfferedTestId)obj;
			if(this.medicalTestName == offeredTestsId.getMedicalTestName() && this.healthCenterName.equals(offeredTestsId.getHealthCenterName())){
			return true;
			}
		}
		else {
			return false;
		}		
		return false;
	}	
	
	@Override
	public int hashCode(){
		return super.hashCode();
	}	   
}
