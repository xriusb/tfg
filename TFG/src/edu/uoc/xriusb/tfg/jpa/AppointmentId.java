package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


@Embeddable
public class AppointmentId implements Serializable {
	
	private static final long serialVersionUID = 1L;
	/*@OneToOne
    @JoinColumn(name = "user_identificator")*/
	private String msuserId;
	private Date date;
	
	public AppointmentId() {
		super();
	}

	public String getMsuserId() {
		return msuserId;
	}

	public void setMsuserId(String msuserId) {
		this.msuserId = msuserId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public boolean equals(Object obj){
		
		if(obj instanceof AppointmentId){
			AppointmentId appointmentId = (AppointmentId)obj;
			if(this.msuserId == appointmentId.getMsuserId() && this.date.equals(appointmentId.getDate())){
			return true;
			}
		}
		else {
			return false;
		}
		
		return false;
	}
	
	@Override
	public int hashCode(){
		return super.hashCode();
	}	
}
