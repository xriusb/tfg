package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class CoveredTestId implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String healthInsuranceCompanyName;
	private String healthCenterName;
	private String medicalTestName;

	public CoveredTestId() {
		super();
	}
	
	public String getHealthInsuranceCompanyName() {
		return healthInsuranceCompanyName;
	}

	public void setHealthInsuranceCompanyName(String healthInsuranceCompanyName) {
		this.healthInsuranceCompanyName = healthInsuranceCompanyName;
	}

	public String getHealthCenterName() {
		return healthCenterName;
	}

	public void setHealthCenterName(String healthCenterName) {
		this.healthCenterName = healthCenterName;
	}

	public String getMedicalTestName() {
		return medicalTestName;
	}

	public void setMedicalTestName(String medicalTestName) {
		this.medicalTestName = medicalTestName;
	}

	@Override
	public boolean equals(Object obj){
		
		if(obj instanceof CoveredTestId){
			CoveredTestId coveredTestId = (CoveredTestId)obj;
			if(this.healthInsuranceCompanyName == coveredTestId.getHealthInsuranceCompanyName() && this.healthCenterName == coveredTestId.getHealthCenterName() 
					&& this.medicalTestName == coveredTestId.getMedicalTestName()){
			return true;
			}
		}
		else {
			return false;
		}
		
		return false;
	}
	
	@Override
	public int hashCode(){
		return super.hashCode();
	}
	
	
   
}
