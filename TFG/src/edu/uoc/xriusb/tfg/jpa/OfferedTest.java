package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="uoctfg.offeredTest")
public class OfferedTest implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private OfferedTestId offeredTestId;
	private Float price;
	private Schedule workingHours;
	
	public OfferedTest() {
		super();
	}
	@Id
	public OfferedTestId getOfferedTestId() {
		return offeredTestId;
	}

	public void setOfferedTestId(OfferedTestId offeredTestId) {
		this.offeredTestId = offeredTestId;
	}
	
	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Schedule getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(Schedule workingHours) {
		this.workingHours = workingHours;
	}   
}
