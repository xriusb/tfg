package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="uoctfg.appointment")
public class Appointment implements Serializable {

	
	private static final long serialVersionUID = 1L;
		
	private AppointmentId appointmentId;
	private HealthCenter healthCenter;
	private MedicalTest medicalTest;
	
	/*@OneToOne
    @JoinColumn(name = "company_id",insertable = false, updatable = false)
    private User user;*/
	
	public Appointment() {
		super();		
	}
	
	@Id
	public AppointmentId getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(AppointmentId appointmentId) {
		this.appointmentId = appointmentId;
	}

	@ManyToOne
	@JoinColumn (name="healthCenter")
	public HealthCenter getHealthCenter() {
		return healthCenter;
	}

	public void setHealthCenter(HealthCenter healthCenter) {
		this.healthCenter = healthCenter;
	}
	@ManyToOne
	@JoinColumn (name="medicalTest")
	public MedicalTest getMedicalTest() {
		return medicalTest;
	}

	public void setMedicalTest(MedicalTest medicalTest) {
		this.medicalTest = medicalTest;
	}
}
