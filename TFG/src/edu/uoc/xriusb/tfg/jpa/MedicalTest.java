package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;


@Entity
@Table(name="uoctfg.medicalTest")
public class MedicalTest implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private String name; 
	private String preparations; 
	private Collection<CoveredTest> coveredTests;
	private Collection<OfferedTest> offeredTests;
	private Collection<Appointment> appointments;
	
	public MedicalTest() {
		super();
	}

	public MedicalTest(String name, String preparations) {
		super();
		this.name = name;
		this.preparations = preparations;
	}

	@Id
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPreparations() {
		return preparations;
	}

	public void setPreparations(String preparations) {
		this.preparations = preparations;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "medicalTestName", foreignKey = @ForeignKey(name = "medicalTest_FK")) 
	public Collection<CoveredTest> getCoveredTests() {
		return coveredTests;
	}

	public void setCoveredTests(Collection<CoveredTest> coveredTests) {
		this.coveredTests = coveredTests;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "medicalTestName", foreignKey = @ForeignKey(name = "medicalTest_FK")) 
	public Collection<OfferedTest> getOfferedTests() {
		return offeredTests;
	}

	public void setOfferedTests(Collection<OfferedTest> offeredTests) {
		this.offeredTests = offeredTests;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "medicalTest", foreignKey = @ForeignKey(name = "medicalTest_FK"))
	public Collection<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(Collection<Appointment> appointments) {
		this.appointments = appointments;
	} 
}
