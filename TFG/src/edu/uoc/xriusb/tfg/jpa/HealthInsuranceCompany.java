package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

@Entity
@Table(name="uoctfg.healthInsuranceCompany")
public class HealthInsuranceCompany implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Location address;
	private String logo;
	private Schedule workingTime;
	private String generalPhoneNum;
	private String authorizationPhoneNum;
	private String generalEmail; 
	private String authorizationEmail;
	private Boolean useReferral;	
	private Collection<HealthInsuranceMember> HealthInsuranceMembers;
	private Collection<CoveredTest> coveredTests;
			
	public HealthInsuranceCompany() {
		super();
	}

	public HealthInsuranceCompany(String name, Location address, String logo, Schedule workingTime,
			String generalPhoneNum, String authorizationPhoneNum, String generalEmail, String authorizationEmail,
			Boolean useReferral) {
		super();
		this.name = name;
		this.address = address;
		this.logo = logo;
		this.workingTime = workingTime;
		this.generalPhoneNum = generalPhoneNum;
		this.authorizationPhoneNum = authorizationPhoneNum;
		this.generalEmail = generalEmail;
		this.authorizationEmail = authorizationEmail;
		this.useReferral = useReferral;
	}

	@Id
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getAddress() {
		return address;
	}

	public void setAddress(Location address) {
		this.address = address;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Schedule getWorkingTime() {
		return workingTime;
	}

	public void setWorkingTime(Schedule workingTime) {
		this.workingTime = workingTime;
	}

	public String getGeneralPhoneNum() {
		return generalPhoneNum;
	}

	public void setGeneralPhoneNum(String generalPhoneNum) {
		this.generalPhoneNum = generalPhoneNum;
	}

	public String getAuthorizationPhoneNum() {
		return authorizationPhoneNum;
	}

	public void setAuthorizationPhoneNum(String authorizationPhoneNum) {
		this.authorizationPhoneNum = authorizationPhoneNum;
	}

	public String getGeneralEmail() {
		return generalEmail;
	}

	public void setGeneralEmail(String generalEmail) {
		this.generalEmail = generalEmail;
	}

	public String getAuthorizationEmail() {
		return authorizationEmail;
	}

	public void setAuthorizationEmail(String authorizationEmail) {
		this.authorizationEmail = authorizationEmail;
	}

	public Boolean getUseReferral() {
		return useReferral;
	}

	public void setUseReferral(Boolean useReferral) {
		this.useReferral = useReferral;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "healthInsuranceCompanyName", foreignKey = @ForeignKey(name = "healthInsuranceCompany_FK")) 
	public Collection<HealthInsuranceMember> getHealthInsuranceMembers() {
		return HealthInsuranceMembers;
	}

	public void setHealthInsuranceMembers(Collection<HealthInsuranceMember> healthInsuranceMembers) {
		HealthInsuranceMembers = healthInsuranceMembers;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "healthInsuranceCompanyName", foreignKey = @ForeignKey(name = "healthInsuranceCompany_FK")) 
	public Collection<CoveredTest> getCoveredTests() {
		return coveredTests;
	}

	public void setCoveredTests(Collection<CoveredTest> coveredTests) {
		this.coveredTests = coveredTests;
	}		
}
