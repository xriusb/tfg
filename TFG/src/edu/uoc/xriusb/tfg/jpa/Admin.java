package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Admin
 *
 */
@Entity
@Table(name="uoctfg.admin")
public class Admin implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private String emailAddress;
	private String pswd;

	public Admin() {
		super();
	}

	public Admin(String emailAddress, String pswd) {
		this.emailAddress = emailAddress;
		this.pswd = pswd;
	}

	@Id
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPswd() {
		return pswd;
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}	
   
}
