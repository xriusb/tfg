package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="uoctfg.healthCenterEmployee")
public class HealthCenterEmployee implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String email;
	private String pswd;
	private HealthCenter healthCenter;

	public HealthCenterEmployee() {
		super();
	}

	public HealthCenterEmployee(String email, String pswd, HealthCenter healthCenter) {
		this.email = email;
		this.pswd = pswd;
		this.healthCenter = healthCenter;
	}

	@Id
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(nullable = false)
	public String getPswd() {
		return pswd;
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	@OneToOne
	@JoinColumn (name="healthCenter", nullable = false, foreignKey = @ForeignKey(name = "healthCenter_FK"))
	public HealthCenter getHealthCenter() {
		return healthCenter;
	}

	public void setHealthCenter(HealthCenter healthCenter) {
		this.healthCenter = healthCenter;
	}   
}
