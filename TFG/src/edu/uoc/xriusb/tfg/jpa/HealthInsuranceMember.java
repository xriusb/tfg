package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="uoctfg.healthInsuranceMember")
public class HealthInsuranceMember  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private HealthInsuranceMemberId healthInsuranceMemberId;
	private String cardNumber;
	
	public HealthInsuranceMember() {
		super();
	}
	@Id
	public HealthInsuranceMemberId getHealthInsuranceMemberId() {
		return healthInsuranceMemberId;
	}
	public void setHealthInsuranceMemberId(HealthInsuranceMemberId healthInsuranceMemberId) {
		this.healthInsuranceMemberId = healthInsuranceMemberId;
	}
	@Column(unique=true)
	public String getCardNumber() {
		return cardNumber;
	}
		
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
}
