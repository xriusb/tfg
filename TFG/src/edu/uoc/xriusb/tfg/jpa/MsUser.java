package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

@Entity
@Table(name="uoctfg.msuser")
public class MsUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String emailAddress;
	private String pswd;
	private String name;
	private String surname;
	private Collection<HealthInsuranceMember> HealthInsuranceMembers;
	private Collection<Appointment> appointments;
	
	public MsUser() {
		super();
	}

	@Id
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}	
	
	public String getPswd() {
		return pswd;
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}	
		
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "msuser", foreignKey = @ForeignKey(name = "msuser_FK"))  
	public Collection<HealthInsuranceMember> getHealthInsuranceMembers() {
		return HealthInsuranceMembers;
	}

	public void setHealthInsuranceMembers(Collection<HealthInsuranceMember> healthInsuranceMembers) {
		HealthInsuranceMembers = healthInsuranceMembers;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "msuserId", foreignKey = @ForeignKey(name = "msuser_FK"))  
	public Collection<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(Collection<Appointment> appointments) {
		this.appointments = appointments;
	}	
}