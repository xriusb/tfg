package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Schedule
 *
 */
@Entity
@Table(name="uoctfg.schedule")
public class Schedule implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id private int id;

	public Schedule() {
		super();
	}
   
}
