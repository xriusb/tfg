package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

@Entity
@Table(name="uoctfg.healthCenter")
public class HealthCenter implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Location address;
	private Schedule workingTime;
	private String generalPhoneNum;
	private String appointmentPhoneNum;
	private String generalEmail;
	private String appointmentSystemLink;
	private Collection<CoveredTest> coveredTests;
	private Collection<OfferedTest> offeredTests;
	private Collection<Appointment> appointments;
	
	public HealthCenter() {
		super();
	}

	public HealthCenter(String name, Location address, Schedule workingTime, String generalPhoneNum,
			String appointmentPhoneNum, String generalEmail, String appointmentSystemLink) {
		super();
		this.name = name;
		this.address = address;
		this.workingTime = workingTime;
		this.generalPhoneNum = generalPhoneNum;
		this.appointmentPhoneNum = appointmentPhoneNum;
		this.generalEmail = generalEmail;
		this.appointmentSystemLink = appointmentSystemLink;
	}

	@Id
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getAddress() {
		return address;
	}

	public void setAddress(Location address) {
		this.address = address;
	}

	public Schedule getWorkingTime() {
		return workingTime;
	}

	public void setWorkingTime(Schedule workingTime) {
		this.workingTime = workingTime;
	}

	public String getGeneralPhoneNum() {
		return generalPhoneNum;
	}

	public void setGeneralPhoneNum(String generalPhoneNum) {
		this.generalPhoneNum = generalPhoneNum;
	}

	public String getAppointmentPhoneNum() {
		return appointmentPhoneNum;
	}

	public void setAppointmentPhoneNum(String appointmentPhoneNum) {
		this.appointmentPhoneNum = appointmentPhoneNum;
	}

	public String getGeneralEmail() {
		return generalEmail;
	}

	public void setGeneralEmail(String generalEmail) {
		this.generalEmail = generalEmail;
	}

	public String getAppointmentSystemLink() {
		return appointmentSystemLink;
	}

	public void setAppointmentSystemLink(String appointmentSystemLink) {
		this.appointmentSystemLink = appointmentSystemLink;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "healthCenterName", foreignKey = @ForeignKey(name = "healthCenter_FK")) 
	public Collection<CoveredTest> getCoveredTests() {
		return coveredTests;
	}

	public void setCoveredTests(Collection<CoveredTest> coveredTests) {
		this.coveredTests = coveredTests;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "healthCenterName", foreignKey = @ForeignKey(name = "healthCenter_FK")) 
	public Collection<OfferedTest> getOfferedTests() {
		return offeredTests;
	}

	public void setOfferedTests(Collection<OfferedTest> offeredTests) {
		this.offeredTests = offeredTests;
	} 

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn(name = "healthCenter", foreignKey = @ForeignKey(name = "healthCenter_FK"))
	public Collection<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(Collection<Appointment> appointments) {
		this.appointments = appointments;
	}  
}
