package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="uoctfg.coveredTest")
public class CoveredTest  implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private CoveredTestId coveredTestId;
	private Boolean authorizationRequired;
	
	public CoveredTest() {
		super();
	}
	
	@Id
	public CoveredTestId getCoveredTestId() {
		return coveredTestId;
	}

	public void setCoveredTestId(CoveredTestId coveredTestId) {
		this.coveredTestId = coveredTestId;
	}

	public Boolean getAuthorizationRequired() {
		return authorizationRequired;
	}

	public void setAuthorizationRequired(Boolean authorizationRequired) {
		this.authorizationRequired = authorizationRequired;
	}
}
