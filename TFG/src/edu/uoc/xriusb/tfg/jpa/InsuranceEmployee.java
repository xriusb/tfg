package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="uoctfg.insuranceEmployee")
public class InsuranceEmployee implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String emailAddress;
	private String pswd;
	private HealthInsuranceCompany healthInsuranceCompany;

	public InsuranceEmployee() {
		super();
	}

	public InsuranceEmployee(String emailAddress, String pswd, HealthInsuranceCompany healthInsuranceCompany) {
		super();
		this.emailAddress = emailAddress;
		this.pswd = pswd;
		this.healthInsuranceCompany = healthInsuranceCompany;
	}

	@Id
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPswd() {
		return pswd;
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	@OneToOne
	@JoinColumn (name="healthInsuranceCompany", nullable = false, foreignKey = @ForeignKey(name = "healthInsuranceCompany_FK"))
	public HealthInsuranceCompany getHealthInsuranceCompany() {
		return healthInsuranceCompany;
	}

	public void setHealthInsuranceCompany(HealthInsuranceCompany healthInsuranceCompany) {
		this.healthInsuranceCompany = healthInsuranceCompany;
	}
	   
}
