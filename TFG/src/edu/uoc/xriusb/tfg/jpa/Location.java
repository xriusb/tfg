package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Location
 *
 */
@Entity
@Table(name="uoctfg.location")
public class Location implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id private int id;

	public Location() {
		super();
	}
   
}
