package edu.uoc.xriusb.tfg.jpa;

import java.io.Serializable;
import javax.persistence.*;

@Embeddable
public class HealthInsuranceMemberId implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String msuser;
	private String healthInsuranceCompany;
	
	public HealthInsuranceMemberId() {
		super();
	}

	public String getMsuser() {
		return msuser;
	}

	public void setMsuser(String msuser) {
		this.msuser = msuser;
	}
	
	public String getHealthInsuranceCompany() {
		return healthInsuranceCompany;
	}

	public void setHealthInsuranceCompany(String healthInsuranceCompany) {
		this.healthInsuranceCompany = healthInsuranceCompany;
	}

	@Override
	public boolean equals(Object obj){
		
		if(obj instanceof HealthInsuranceMemberId){
			HealthInsuranceMemberId healthInsuranceMembersId = (HealthInsuranceMemberId)obj;
			if(this.msuser == healthInsuranceMembersId.getMsuser() && this.healthInsuranceCompany.equals(healthInsuranceMembersId.getHealthInsuranceCompany())){
			return true;
			}
		}
		else {
			return false;
		}		
		return false;
	}	
	
	@Override
	public int hashCode(){
		return super.hashCode();
	}	
	
   
}
